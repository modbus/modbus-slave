# Modbus Slave
This library is for making it easier to communicate with the [Modbus Broker](https://gitlab.com/modbus/modbus-broker). For more information on how the [Modbus Broker](https://gitlab.com/modbus/modbus-broker) and the Modbus Slave should be connected click [here](https://gitlab.com/modbus/modbus-broker). For an example on how to use this library view the example app [here](https://gitlab.com/modbus/modbus-slave-example).

## Build and Include Library
Install the library into your local maven repository:
```sh
$ git clone https://gitlab.com/modbus/modbus-slave.git
$ cd modbus-slave
$ gradle clean install
```

Include the library in your gradle project:
```gradle
repositories {
    mavenLocal()
}

dependencies {
    compile 'com.gitlab.soshibby.modbus:modbus-slave:1.0-SNAPSHOT'
}
```

Include the library in your maven project:
```xml
<dependency>
    <groupId>com.gitlab.soshibby.modbus</groupId>
    <artifactId>modbus-slave</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

## Example Project
[Modbus Slave Example](https://gitlab.com/modbus/modbus-slave-example)

## License
MIT License

Copyright (c) 2018 Henrik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.