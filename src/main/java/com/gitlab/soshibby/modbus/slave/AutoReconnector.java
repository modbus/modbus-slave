package com.gitlab.soshibby.modbus.slave;

import com.gitlab.soshibby.modbus.slave.clients.ModBusClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AutoReconnector implements Runnable {

    private final Logger log = LoggerFactory.getLogger(AutoReconnector.class);
    private Thread reconnectThread;
    private ModBusClient client;

    public AutoReconnector(ModBusClient client) {
        this.client = client;
    }

    public void enableAutoReconnect() {
        reconnectThread = new Thread(this);
        reconnectThread.start();
    }

    public void disableAutoReconnect() {
        reconnectThread.interrupt();
        reconnectThread = null;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            if (!client.isConnected()) {
                log.info("Client isn't connected, trying to reconnect...");

                try {
                    client.connect();
                } catch (IOException e) {
                    log.error("Failed to connect.", e);
                }
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error("An error occurred while trying to sleep.", e);
            }
        }
    }
}
