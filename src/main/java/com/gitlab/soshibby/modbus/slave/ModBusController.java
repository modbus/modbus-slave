package com.gitlab.soshibby.modbus.slave;

import com.gitlab.soshibby.modbus.slave.events.MessageEventListener;
import com.gitlab.soshibby.modbus.slave.exceptions.IllegalDataAddressException;
import com.gitlab.soshibby.modbus.slave.factories.ResponseFactory;
import com.gitlab.soshibby.modbus.slave.parsers.DefaultMessageParser;
import com.gitlab.soshibby.modbus.slave.parsers.ModBusMessageParser;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadDiscreteInputRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadInputRegisterRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteMultipleHoldingRegisterRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteSingleCoilRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteSingleHoldingRequest;
import com.gitlab.soshibby.modbus.slave.clients.ModBusClient;
import com.gitlab.soshibby.modbus.slave.exceptions.ModBusException;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadCoilsRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadHoldingRegisterRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteMultipleCoilsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;

public class ModBusController implements MessageEventListener {

    private final Logger log = LoggerFactory.getLogger(ModBusController.class);
    private ResponseFactory responseFactory = new ResponseFactory();

    private boolean[] coils = new boolean[10];
    private int[] holdingRegisters = new int[10];
    private boolean[] discreteInputs = new boolean[10];
    private int[] inputRegisters = new int[10];

    public void start() throws IOException {
        ModBusMessageParser parser = new DefaultMessageParser();
        ModBusClient client = new ModBusClient("127.0.0.1", 503, parser);
        client.addListener(this);
        client.connect();

        new AutoReconnector(client).enableAutoReconnect();
    }

    @Override
    public void onMessage(ModBusClient client, ModBusMessage request) {
        try {
            ModBusMessage response = handleRequest(request);
            sendResponse(client, response);
        } catch (ModBusException e) {
            log.error("An error occurred while handling modbus messages.", e);
            ModBusMessage response = responseFactory.createExceptionResponse(request.getHeader(), e);
            sendResponse(client, response);
        } catch (Exception e) {
            log.error("An unexpected error occurred while handling modbus request.", e);
        }
    }

    private ModBusMessage handleRequest(ModBusMessage request) {
        if (request instanceof ReadCoilsRequest) {
            return handleRequest((ReadCoilsRequest) request);
        } else if (request instanceof ReadDiscreteInputRequest) {
            return handleRequest((ReadDiscreteInputRequest) request);
        } else if (request instanceof ReadInputRegisterRequest) {
            return handleRequest((ReadInputRegisterRequest) request);
        } else if (request instanceof ReadHoldingRegisterRequest) {
            return handleRequest((ReadHoldingRegisterRequest) request);
        } else if (request instanceof WriteSingleCoilRequest) {
            return handleRequest((WriteSingleCoilRequest) request);
        } else if (request instanceof WriteSingleHoldingRequest) {
            return handleRequest((WriteSingleHoldingRequest) request);
        } else if (request instanceof WriteMultipleCoilsRequest) {
            return handleRequest((WriteMultipleCoilsRequest) request);
        } else if (request instanceof WriteMultipleHoldingRegisterRequest) {
            return handleRequest((WriteMultipleHoldingRegisterRequest) request);
        } else {
            log.warn("Unknown messages type received: " + request.getClass().getSimpleName());
            return null;
        }
    }

    private ModBusMessage handleRequest(ReadCoilsRequest request) {
        if (request.getStartingAddress() + request.getQuantity() > coils.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of coils bounds.");
        }

        boolean[] values = readBooleans(coils, request.getStartingAddress(), request.getQuantity());

        return responseFactory.createReadCoilsResponse(request, values);
    }

    private ModBusMessage handleRequest(ReadDiscreteInputRequest request) {
        if (request.getStartingAddress() + request.getQuantity() > discreteInputs.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of discrete input registers bounds.");
        }

        boolean[] values = readBooleans(discreteInputs, request.getStartingAddress(), request.getQuantity());

        return responseFactory.createReadDiscreteInputResponse(request, values);
    }

    private ModBusMessage handleRequest(ReadInputRegisterRequest request) {
        if (request.getStartingAddress() + request.getQuantity() > inputRegisters.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of input register bounds.");
        }

        int[] values = readInts(inputRegisters, request.getStartingAddress(), request.getQuantity());

        return responseFactory.createReadInputRegisterResponse(request, values);
    }

    private ModBusMessage handleRequest(ReadHoldingRegisterRequest request) {
        if (request.getStartingAddress() + request.getQuantity() > holdingRegisters.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of holding register bounds.");
        }

        int[] values = readInts(holdingRegisters, request.getStartingAddress(), request.getQuantity());

        return responseFactory.createReadHoldingRegisterResponse(request, values);
    }

    private ModBusMessage handleRequest(WriteSingleCoilRequest request) {
        if (request.getStartingAddress() > coils.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of coils bounds.");
        }

        write(request.getValue(), coils, request.getStartingAddress());

        return responseFactory.createWriteSingleCoilResponse(request);
    }

    private ModBusMessage handleRequest(WriteSingleHoldingRequest request) {
        if (request.getStartingAddress() > holdingRegisters.length) {
            throw new IllegalDataAddressException("Starting address is outside of holding of register bounds.");
        }

        write(request.getValue(), holdingRegisters, request.getStartingAddress());

        return responseFactory.createWriteSingleHoldingRegisterResponse(request);
    }

    private ModBusMessage handleRequest(WriteMultipleCoilsRequest request) {
        if (request.getStartingAddress() + request.getQuantity() > coils.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of coils bounds.");
        }

        write(request.getValues(), coils, request.getStartingAddress());

        return responseFactory.createWriteMultipleCoilsResponse(request);
    }

    private ModBusMessage handleRequest(WriteMultipleHoldingRegisterRequest request) {
        if (request.getStartingAddress() + request.getQuantity() > holdingRegisters.length) {
            throw new IllegalDataAddressException("Starting address + quantity is outside of holding register bounds.");
        }

        write(request.getValues(), holdingRegisters, request.getStartingAddress());

        return responseFactory.createWriteMultipleHoldingRegisterResponse(request);
    }

    private void sendResponse(ModBusClient client, ModBusMessage response) {
        if (response == null) {
            log.warn("No response sent for messages: " + response.toByte());
        }
        try {
            client.send(response);
        } catch (IOException e) {
            log.error("Failed to send message: " + response.toByte());
        }
    }

    private void write(int source, int[] destination, int startingPosition) {
        destination[startingPosition] = source;
    }

    private void write(boolean source, boolean[] destination, int startingPosition) {
        destination[startingPosition] = source;
    }

    private void write(boolean[] source, boolean[] destination, int startingPosition) {
        for (int i = 0; i < source.length; i++) {
            destination[startingPosition + i] = source[i];
        }
    }

    private void write(int[] source, int[] destination, int startingPosition) {
        for (int i = 0; i < source.length; i++) {
            destination[startingPosition + i] = source[i];
        }
    }

    private int[] readInts(int[] source, int startingPosition, int count) {
        return Arrays.copyOfRange(source, startingPosition, startingPosition + count);
    }

    private boolean[] readBooleans(boolean[] source, int startingPosition, int count) {
        return Arrays.copyOfRange(source, startingPosition, startingPosition + count);
    }

}
