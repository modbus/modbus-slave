package com.gitlab.soshibby.modbus.slave.clients;

import com.gitlab.soshibby.modbus.slave.events.MessageEventListener;
import com.gitlab.soshibby.modbus.slave.exceptions.ModBusException;
import com.gitlab.soshibby.modbus.slave.factories.ResponseFactory;
import com.gitlab.soshibby.modbus.slave.parsers.HeaderParser;
import com.gitlab.soshibby.modbus.slave.parsers.ModBusMessageParser;
import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModBusClient extends TCPClient {

    private final Logger log = LoggerFactory.getLogger(ModBusClient.class);
    private List<MessageEventListener> listeners = new ArrayList<>();
    private HeaderParser headerParser = new HeaderParser();
    private int HEADER_LENGTH = 8;
    private ResponseFactory responseFactory = new ResponseFactory();
    private ModBusMessageParser messageParser;

    public ModBusClient(String ip, int port, ModBusMessageParser messageParser) {
        super(ip, port);
        this.messageParser = messageParser;
    }

    @Override
    protected void handleMessage(byte[] data) {
        log.info("Parsing modbus header.");
        ModBusHeader header = parseHeader(data);

        log.info("Parsing modbus body.");
        byte[] body = getBody(data);
        ModBusMessage message = parseBody(header, body);

        log.info("Notifying modbus message listeners");
        if (message != null) {
            sendMessageEvent(message);
        }
    }

    private ModBusHeader parseHeader(byte[] data) {
        return headerParser.parse(data);
    }

    private ModBusMessage parseBody(ModBusHeader header, byte[] data) {
        try {
            return messageParser.parse(header, data);
        } catch (ModBusException e) {
            log.error("An error occurred while parsing modbus message.", e);
            sendExceptionResponse(e, header);
        } catch (Exception e) {
            log.error("An unexpected error occurred while parsing modbus message.", e);
        }

        return null;
    }

    private void sendExceptionResponse(ModBusException exception, ModBusHeader header) {
        ModBusMessage response = responseFactory.createExceptionResponse(header, exception);

        try {
            send(response);
        } catch (IOException e) {
            log.error("Failed to send modbus exception response.", e);
        }
    }

    private byte[] getBody(byte[] data) {
        int size = data.length - HEADER_LENGTH;
        byte[] message = new byte[size];
        System.arraycopy(data, HEADER_LENGTH, message, 0, size);
        return message;
    }

    private void sendMessageEvent(ModBusMessage message) {
        listeners.forEach(listener -> listener.onMessage(this, message));
    }

    public void send(ModBusMessage message) throws IOException {
        send(message.toByte());
    }

    public void addListener(MessageEventListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MessageEventListener listener) {
        listeners.remove(listener);
    }

}
