package com.gitlab.soshibby.modbus.slave.clients;

import com.gitlab.soshibby.modbus.slave.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

public abstract class TCPClient implements Runnable {
    private final Logger log = LoggerFactory.getLogger(TCPClient.class);
    private Socket socket;
    private Thread thread;
    private String ip;
    private int port;

    public TCPClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public synchronized void connect() throws IOException {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    public boolean isConnected() {
        return socket != null && socket.isConnected() && !socket.isClosed();
    }

    public void disconnect() {
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                log.error("Failed to close socket.", e);
            }

            socket = null;
        }

        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
    }

    public void run() {
        try {
            socket = new Socket(ip, port);
            InputStream inputStream = socket.getInputStream();

            log.info("Connected!");

            while (!Thread.currentThread().isInterrupted()) {
                byte[] data = receive(inputStream);
                handleMessage(data);
            }

            disconnect();
        } catch (IOException e) {
            log.error("A socket error occurred.", e);
            disconnect();
        }
    }

    protected abstract void handleMessage(byte[] message);

    private byte[] receive(InputStream inputStream) throws IOException {
        byte[] data = new byte[2100];
        int numberOfBytes = inputStream.read(data, 0, data.length);

        // -1 indicates that the remote part has closed the connection.
        if (numberOfBytes == -1) {
            disconnect();
        }

        byte[] receivedData = Arrays.copyOf(data, numberOfBytes);
        log.debug("Received data: " + Util.bytesToHex(receivedData));
        return receivedData;
    }

    public void send(byte[] data) throws IOException {
        if (socket.isConnected() & !socket.isClosed()) {
            log.debug("Sending data: " + Util.bytesToHex(data));
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(data);
            outputStream.flush();
        } else {
            log.error("Failed to send data. Socket is not connected.");
            throw new IOException("Socket is not connected.");
        }
    }

}
