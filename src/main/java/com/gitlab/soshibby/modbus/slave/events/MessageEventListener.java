package com.gitlab.soshibby.modbus.slave.events;

import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.clients.ModBusClient;

public interface MessageEventListener {
    void onMessage(ModBusClient client, ModBusMessage request);
}
