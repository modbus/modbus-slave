package com.gitlab.soshibby.modbus.slave.exceptions;

public class IllegalDataAddressException extends ModBusException {

    public IllegalDataAddressException(String message) {
        super(message);
    }

    @Override
    public byte getExceptionCode() {
        return 2;
    }

}
