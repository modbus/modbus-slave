package com.gitlab.soshibby.modbus.slave.exceptions;

public class IllegalDataValueException extends ModBusException {

    public IllegalDataValueException(String message) {
        super(message);
    }

    @Override
    public byte getExceptionCode() {
        return 3;
    }

}
