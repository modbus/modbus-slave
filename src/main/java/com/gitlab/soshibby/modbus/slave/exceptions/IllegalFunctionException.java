package com.gitlab.soshibby.modbus.slave.exceptions;

public class IllegalFunctionException extends ModBusException {

    public IllegalFunctionException(String message) {
        super(message);
    }

    @Override
    public byte getExceptionCode() {
        return 1;
    }

}
