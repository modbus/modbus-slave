package com.gitlab.soshibby.modbus.slave.exceptions;

public class IllegalResponseLengthException extends ModBusException {

    public IllegalResponseLengthException(String message) {
        super(message);
    }

    @Override
    public byte getExceptionCode() {
        return 4;
    }

}
