package com.gitlab.soshibby.modbus.slave.exceptions;

public abstract class ModBusException extends RuntimeException {

    public ModBusException(String message) {
        super(message);
    }

    public abstract byte getExceptionCode();

}
