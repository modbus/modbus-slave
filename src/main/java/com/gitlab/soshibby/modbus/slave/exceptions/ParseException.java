package com.gitlab.soshibby.modbus.slave.exceptions;

public class ParseException extends RuntimeException {

    public ParseException(String message) {
        super(message);
    }

}
