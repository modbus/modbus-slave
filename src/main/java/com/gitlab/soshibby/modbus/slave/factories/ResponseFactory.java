package com.gitlab.soshibby.modbus.slave.factories;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadDiscreteInputRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadInputRegisterRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteMultipleHoldingRegisterRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteSingleCoilRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteSingleHoldingRequest;
import com.gitlab.soshibby.modbus.slave.types.responses.ExceptionResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.ReadDiscreteInputResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.ReadHoldingRegisterResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.ReadInputRegisterResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.WriteMultipleCoilsResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.WriteMultipleHoldingRegisterResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.WriteSingleCoilResponse;
import com.gitlab.soshibby.modbus.slave.types.responses.WriteSingleHoldingResponse;
import com.gitlab.soshibby.modbus.slave.exceptions.ModBusException;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadCoilsRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadHoldingRegisterRequest;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteMultipleCoilsRequest;
import com.gitlab.soshibby.modbus.slave.types.responses.ReadCoilsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseFactory {

    private static final Logger log = LoggerFactory.getLogger(ResponseFactory.class);

    public ReadCoilsResponse createReadCoilsResponse(ReadCoilsRequest request, boolean[] values) {
        log.info("Creating read coils response with values {}.", values);
        ReadCoilsResponse response = new ReadCoilsResponse(request.getHeader());

        if (request.getQuantity() != values.length) {
            throw new RuntimeException("Requested coils (" + request.getQuantity() + ") and coils sent (" + values.length + ") doesn't match.");
        }

        if (request.getQuantity() % 8 == 0) {
            response.setByteCount((byte) (request.getQuantity() / 8));
        } else {
            response.setByteCount((byte) (request.getQuantity() / 8 + 1));
        }

        response.setValues(values);

        return response;
    }

    public ReadDiscreteInputResponse createReadDiscreteInputResponse(ReadDiscreteInputRequest request, boolean[] values) {
        log.info("Creating read discrete input response with values {}.", values);
        ReadDiscreteInputResponse response = new ReadDiscreteInputResponse(request.getHeader());

        if (request.getQuantity() != values.length) {
            throw new RuntimeException("Requested discrete values (" + request.getQuantity() + ") and discrete values sent (" + values.length + ") doesn't match.");
        }

        if ((request.getQuantity() % 8) == 0) {
            response.setByteCount((byte) (request.getQuantity() / 8));
        } else {
            response.setByteCount((byte) (request.getQuantity() / 8 + 1));
        }

        response.setValues(values);

        return response;
    }

    public ReadInputRegisterResponse createReadInputRegisterResponse(ReadInputRegisterRequest request, int[] values) {
        log.info("Creating read input register response with values {}.", values);
        ReadInputRegisterResponse response = new ReadInputRegisterResponse(request.getHeader());

        if (request.getQuantity() != values.length) {
            throw new RuntimeException("Requested discrete values (" + request.getQuantity() + ") and discrete values sent (" + values.length + ") doesn't match.");
        }

        response.setByteCount((byte) (2 * request.getQuantity()));
        response.setValues(values);

        return response;
    }

    public ReadHoldingRegisterResponse createReadHoldingRegisterResponse(ReadHoldingRegisterRequest request, int[] values) {
        log.info("Creating read holding register response with values {}.", values);
        ReadHoldingRegisterResponse response = new ReadHoldingRegisterResponse(request.getHeader());

        if (request.getQuantity() != values.length) {
            throw new RuntimeException("Requested holding values (" + request.getQuantity() + ") and holding values sent (" + values.length + ") doesn't match.");
        }

        response.setByteCount((byte) (2 * request.getQuantity()));
        response.setValues(values);

        return response;
    }

    public WriteSingleCoilResponse createWriteSingleCoilResponse(WriteSingleCoilRequest request) {
        log.info("Creating write single coil response.");
        WriteSingleCoilResponse response = new WriteSingleCoilResponse(request.getHeader());

        response.setStartingAddress(request.getStartingAddress());
        response.setValue(request.getValue());

        return response;
    }

    public WriteSingleHoldingResponse createWriteSingleHoldingRegisterResponse(WriteSingleHoldingRequest request) {
        log.info("Creating write single holding register response.");
        WriteSingleHoldingResponse response = new WriteSingleHoldingResponse(request.getHeader());
        response.setStartingAddress(request.getStartingAddress());
        response.setValue(request.getValue());

        return response;
    }

    public WriteMultipleCoilsResponse createWriteMultipleCoilsResponse(WriteMultipleCoilsRequest request) {
        log.info("Creating write multiple coils response.");
        WriteMultipleCoilsResponse response = new WriteMultipleCoilsResponse(request.getHeader());

        response.setStartingAddress(request.getStartingAddress());
        response.setQuantity(request.getQuantity());

        return response;
    }

    public WriteMultipleHoldingRegisterResponse createWriteMultipleHoldingRegisterResponse(WriteMultipleHoldingRegisterRequest request) {
        log.info("Creating write multiple holding register response.");
        WriteMultipleHoldingRegisterResponse response = new WriteMultipleHoldingRegisterResponse(request.getHeader());

        response.setStartingAddress(request.getStartingAddress());
        response.setQuantity(request.getQuantity());

        return response;
    }

    public ExceptionResponse createExceptionResponse(ModBusHeader header, ModBusException exception) {
        log.info("Creating exception response.");
        ExceptionResponse response = new ExceptionResponse(header);

        response.setErrorCode((byte) (header.getFunctionCode() + 0x80));
        response.setExceptionCode(exception.getExceptionCode());

        return response;
    }

}
