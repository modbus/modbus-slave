package com.gitlab.soshibby.modbus.slave.parsers;

import com.gitlab.soshibby.modbus.slave.parsers.messages.WriteMultipleCoilsParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.WriteMultipleHoldingRegisterParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.ReadCoilsParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.ReadDiscreteInputParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.ReadHoldingRegisterParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.ReadInputRegisterParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.WriteSingleCoilParser;
import com.gitlab.soshibby.modbus.slave.parsers.messages.WriteSingleHoldingRegisterParser;

public class DefaultMessageParser extends MessageParser {

    public DefaultMessageParser() {
        addParser(1, ReadCoilsParser::parse);
        addParser(2, ReadDiscreteInputParser::parse);
        addParser(3, ReadHoldingRegisterParser::parse);
        addParser(4, ReadInputRegisterParser::parse);
        addParser(5, WriteSingleCoilParser::parse);
        addParser(6, WriteSingleHoldingRegisterParser::parse);
        addParser(15, WriteMultipleCoilsParser::parse);
        addParser(16, WriteMultipleHoldingRegisterParser::parse);
    }

}
