package com.gitlab.soshibby.modbus.slave.parsers;

import com.gitlab.soshibby.modbus.slave.exceptions.ParseException;
import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;

public class HeaderParser {

    public ModBusHeader parse(byte[] message) {
        BufferReader reader = new BufferReader(message);
        ModBusHeader header = new ModBusHeader();

        assertHasNextShort(reader, "transaction identifier not found.");
        header.setTransactionIdentifier(reader.nextUnsignedShort());

        assertHasNextShort(reader, "protocol identifier not found.");
        header.setProtocolIdentifier(reader.nextUnsignedShort());

        assertHasNextShort(reader, "length not found.");
        header.setLength(reader.nextUnsignedShort());

        assertHasNextByte(reader, "unit identifier not found.");
        header.setUnitIdentifier(reader.nextUnsignedByte());

        assertHasNextByte(reader, "function code not found.");
        header.setFunctionCode(reader.nextUnsignedByte());

        return header;
    }

    private void assertHasNextShort(BufferReader reader, String onFailMessage) {
        if (!reader.hasNextShort()) {
            throw new ParseException("Failed to parse header, " + onFailMessage);
        }
    }

    private void assertHasNextByte(BufferReader reader, String onFailMessage) {
        if (!reader.hasNextByte()) {
            throw new ParseException("Failed to parse header, " + onFailMessage);
        }
    }

}
