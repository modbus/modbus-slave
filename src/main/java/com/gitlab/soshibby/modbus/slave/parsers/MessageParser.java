package com.gitlab.soshibby.modbus.slave.parsers;

import com.gitlab.soshibby.modbus.slave.exceptions.IllegalFunctionException;
import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.exceptions.IllegalDataValueException;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public abstract class MessageParser implements ModBusMessageParser {

    private static final Logger log = LoggerFactory.getLogger(MessageParser.class);
    private static Map<Short, BiFunction<ModBusHeader, BufferReader, ModBusMessage>> parsers = new HashMap<>();

    public void addParser(int functionCode, BiFunction<ModBusHeader, BufferReader, ModBusMessage> parser) {
        if (functionCode > 255) {
            throw new IllegalArgumentException("Function code cannot be greater than 255.");
        }

        parsers.put((short) functionCode, parser);
    }

    public ModBusMessage parse(ModBusHeader header, byte[] body) {
        log.info("Parsing modbus body.");
        BufferReader reader = new BufferReader(body);
        short functionCode = header.getFunctionCode();

        if (canParse(functionCode)) {
            log.debug("Parsing modbus message with function code {}.", functionCode);
            ModBusMessage request = parse(functionCode, header, reader);

            if (!reader.isEndOfBuffer()) {
                log.error("Expected end of message, but found additional data.");
                throw new IllegalDataValueException("Expected end of message, but found additional data.");
            }

            return request;
        } else {
            log.error("Function code '" + functionCode + "' is not supported.");
            throw new IllegalFunctionException("Function code '" + functionCode + "' is not supported.");
        }
    }

    private boolean canParse(short functionCode) {
        return parsers.containsKey(functionCode);
    }

    private ModBusMessage parse(short functionCode, ModBusHeader header, BufferReader reader) {
        return parsers.get(functionCode).apply(header, reader);
    }

}
