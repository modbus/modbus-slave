package com.gitlab.soshibby.modbus.slave.parsers;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;

public interface ModBusMessageParser {
    ModBusMessage parse(ModBusHeader header, byte[] data);
}
