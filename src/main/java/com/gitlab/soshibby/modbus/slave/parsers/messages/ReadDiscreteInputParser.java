package com.gitlab.soshibby.modbus.slave.parsers.messages;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadDiscreteInputRequest;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadDiscreteInputParser {

    private static final Logger log = LoggerFactory.getLogger(ReadDiscreteInputParser.class);

    public static ModBusMessage parse(ModBusHeader header, BufferReader reader) {
        log.info("Parsing read discrete input request.");
        ReadDiscreteInputRequest request = new ReadDiscreteInputRequest(header);

        BufferReaderAsserter.assertHasNextByte(reader, "Starting address not found.");
        request.setStartingAddress(reader.nextUnsignedShort());

        BufferReaderAsserter.assertHasNextShort(reader, "Quantity not found.");
        request.setQuantity(reader.nextUnsignedShort());

        return request;
    }

}
