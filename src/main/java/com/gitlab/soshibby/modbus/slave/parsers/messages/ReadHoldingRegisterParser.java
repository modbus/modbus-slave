package com.gitlab.soshibby.modbus.slave.parsers.messages;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadHoldingRegisterRequest;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextByte;
import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextShort;

public class ReadHoldingRegisterParser {

    private static final Logger log = LoggerFactory.getLogger(ReadHoldingRegisterParser.class);

    public static ModBusMessage parse(ModBusHeader header, BufferReader reader) {
        log.info("Parsing read holding register request.");
        ReadHoldingRegisterRequest request = new ReadHoldingRegisterRequest(header);

        assertHasNextByte(reader, "Starting address not found.");
        request.setStartingAddress(reader.nextUnsignedShort());

        assertHasNextShort(reader, "Quantity not found.");
        request.setQuantity(reader.nextUnsignedShort());

        return request;
    }

}
