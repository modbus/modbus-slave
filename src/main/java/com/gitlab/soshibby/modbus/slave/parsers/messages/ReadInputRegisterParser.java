package com.gitlab.soshibby.modbus.slave.parsers.messages;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.requests.ReadInputRegisterRequest;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextByte;
import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextShort;

public class ReadInputRegisterParser {

    private static final Logger log = LoggerFactory.getLogger(ReadInputRegisterParser.class);

    public static ModBusMessage parse(ModBusHeader header, BufferReader reader) {
        log.info("Parsing read input register request.");
        ReadInputRegisterRequest request = new ReadInputRegisterRequest(header);

        assertHasNextByte(reader, "Starting address not found.");
        request.setStartingAddress(reader.nextUnsignedShort());

        assertHasNextShort(reader, "Quantity not found.");
        request.setQuantity(reader.nextUnsignedShort());

        return request;
    }

}
