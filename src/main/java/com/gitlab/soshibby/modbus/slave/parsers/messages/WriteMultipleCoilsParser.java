package com.gitlab.soshibby.modbus.slave.parsers.messages;

import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteMultipleCoilsRequest;
import com.gitlab.soshibby.modbus.slave.utils.BitReader;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextByte;
import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextShort;

public class WriteMultipleCoilsParser {

    private static final Logger log = LoggerFactory.getLogger(WriteMultipleCoilsParser.class);

    public static ModBusMessage parse(ModBusHeader header, BufferReader reader) {
        log.info("Parsing write multiple coils request.");
        WriteMultipleCoilsRequest request = new WriteMultipleCoilsRequest(header);

        assertHasNextByte(reader, "Starting address not found.");
        request.setStartingAddress(reader.nextUnsignedShort());

        assertHasNextShort(reader, "Quantity not found.");
        request.setQuantity(reader.nextUnsignedShort());

        assertHasNextByte(reader, "ByteCount not found.");
        request.setByteCount(reader.nextUnsignedByte());

        request.setValues(readBoolArray(reader, request.getQuantity()));

        return request;
    }

    private static boolean[] readBoolArray(BufferReader reader, int count) {
        boolean[] result = new boolean[count];
        int i = 0;

        assertHasNextByte(reader, "Value not found.");
        byte currentByte = reader.nextByte();
        BitReader bitReader = new BitReader(currentByte);

        while (i < count) {
            // Fetch the next byte if we are at the end of the current byte.
            if (!bitReader.hasNext()) {
                assertHasNextByte(reader, "Value not found.");
                currentByte = reader.nextByte();
                bitReader = new BitReader(currentByte);
            }

            result[i++] = bitReader.nextBoolean();
        }

        return result;
    }

}
