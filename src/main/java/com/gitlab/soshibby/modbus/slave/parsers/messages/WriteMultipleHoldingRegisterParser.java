package com.gitlab.soshibby.modbus.slave.parsers.messages;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteMultipleHoldingRegisterRequest;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextByte;
import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextShort;

public class WriteMultipleHoldingRegisterParser {

    private static final Logger log = LoggerFactory.getLogger(WriteMultipleHoldingRegisterParser.class);

    public static ModBusMessage parse(ModBusHeader header, BufferReader reader) {
        log.info("Parsing write multiple holding register request.");
        WriteMultipleHoldingRegisterRequest request = new WriteMultipleHoldingRegisterRequest(header);

        assertHasNextByte(reader, "Starting address not found.");
        request.setStartingAddress(reader.nextUnsignedShort());

        assertHasNextShort(reader, "Quantity not found.");
        request.setQuantity(reader.nextUnsignedShort());

        assertHasNextByte(reader, "ByteCount not found.");
        request.setByteCount(reader.nextUnsignedByte());

        request.setValues(new int[request.getQuantity()]);

        for (int i = 0; i < request.getQuantity(); i++) {
            assertHasNextByte(reader, "Value not found, expected to find '" + request.getQuantity() + "' values found '" + i + "' value(s).");
            request.getValues()[i] = reader.nextUnsignedShort();
        }

        return request;
    }

}
