package com.gitlab.soshibby.modbus.slave.parsers.messages;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.requests.WriteSingleHoldingRequest;
import com.gitlab.soshibby.modbus.slave.utils.BufferReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextByte;
import static com.gitlab.soshibby.modbus.slave.utils.BufferReaderAsserter.assertHasNextShort;

public class WriteSingleHoldingRegisterParser {

    private static final Logger log = LoggerFactory.getLogger(WriteSingleHoldingRegisterParser.class);

    public static ModBusMessage parse(ModBusHeader header, BufferReader reader) {
        log.info("Parsing single holding register request.");
        WriteSingleHoldingRequest request = new WriteSingleHoldingRequest(header);

        assertHasNextByte(reader, "Starting address not found.");
        request.setStartingAddress(reader.nextUnsignedShort());

        assertHasNextShort(reader, "Value not found.");
        request.setValue(reader.nextUnsignedShort());

        return request;
    }

}
