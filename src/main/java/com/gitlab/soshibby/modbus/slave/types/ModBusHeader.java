package com.gitlab.soshibby.modbus.slave.types;

public class ModBusHeader {

    protected int transactionIdentifier;
    protected int protocolIdentifier;
    protected short unitIdentifier;
    protected int length;
    protected short functionCode;

    public int getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(int transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public int getProtocolIdentifier() {
        return protocolIdentifier;
    }

    public void setProtocolIdentifier(int protocolIdentifier) {
        this.protocolIdentifier = protocolIdentifier;
    }

    public short getUnitIdentifier() {
        return unitIdentifier;
    }

    public void setUnitIdentifier(short unitIdentifier) {
        this.unitIdentifier = unitIdentifier;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public short getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(short functionCode) {
        this.functionCode = functionCode;
    }

}
