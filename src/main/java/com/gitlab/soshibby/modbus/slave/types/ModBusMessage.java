package com.gitlab.soshibby.modbus.slave.types;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public abstract class ModBusMessage {
    protected ModBusHeader header;

    public ModBusMessage(ModBusHeader header) {
        this.header = header;
    }

    public ModBusHeader getHeader() {
        return header;
    }

    public void setHeader(ModBusHeader header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return "ModBusMessage{" +
                "transaction id=" + header.transactionIdentifier +
                ", protocol id=" + header.protocolIdentifier +
                ", unit id=" + header.unitIdentifier +
                ", function code=" + header.functionCode +
                '}';
    }

    public byte[] toByte() {
        throw new NotImplementedException();
    }

}

