package com.gitlab.soshibby.modbus.slave.types.requests;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;

public class ReadDiscreteInputRequest extends ModBusMessage {

    private int startingAddress;
    private int quantity;

    public ReadDiscreteInputRequest(ModBusHeader header) {
        super(header);
    }

    public int getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress = startingAddress;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
