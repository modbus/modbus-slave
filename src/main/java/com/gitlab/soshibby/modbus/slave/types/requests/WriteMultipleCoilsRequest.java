package com.gitlab.soshibby.modbus.slave.types.requests;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;

public class WriteMultipleCoilsRequest extends ModBusMessage {

    private int startingAddress;
    private int quantity;
    private short byteCount;
    private boolean[] values;

    public WriteMultipleCoilsRequest(ModBusHeader header) {
        super(header);
    }

    public int getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress = startingAddress;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public short getByteCount() {
        return byteCount;
    }

    public void setByteCount(short byteCount) {
        this.byteCount = byteCount;
    }

    public boolean[] getValues() {
        return values;
    }

    public void setValues(boolean[] values) {
        this.values = values;
    }

}
