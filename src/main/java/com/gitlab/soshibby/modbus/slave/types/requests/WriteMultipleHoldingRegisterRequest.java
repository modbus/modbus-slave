package com.gitlab.soshibby.modbus.slave.types.requests;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;

public class WriteMultipleHoldingRegisterRequest extends ModBusMessage {

    private int startingAddress;
    private int quantity;
    private short byteCount;
    private int[] values;

    public WriteMultipleHoldingRegisterRequest(ModBusHeader header) {
        super(header);
    }

    public int getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress = startingAddress;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public short getByteCount() {
        return byteCount;
    }

    public void setByteCount(short byteCount) {
        this.byteCount = byteCount;
    }

    public int[] getValues() {
        return values;
    }

    public void setValues(int[] values) {
        this.values = values;
    }
}
