package com.gitlab.soshibby.modbus.slave.types.requests;

import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;

public class WriteSingleCoilRequest extends ModBusMessage {

    private int startingAddress;
    private boolean value;

    public WriteSingleCoilRequest(ModBusHeader header) {
        super(header);
    }

    public int getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress = startingAddress;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

}
