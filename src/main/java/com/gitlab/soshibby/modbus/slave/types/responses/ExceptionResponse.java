package com.gitlab.soshibby.modbus.slave.types.responses;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.utils.BufferWriter;

public class ExceptionResponse extends ModBusMessage {

    private byte exceptionCode;
    private byte errorCode;

    public ExceptionResponse(ModBusHeader header) {
        super(header);
    }

    public byte getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(byte exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public byte getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(byte errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public byte[] toByte() {
        BufferWriter writer = new BufferWriter(9);

        writer.write(header.getTransactionIdentifier());
        writer.write(header.getProtocolIdentifier());
        writer.write(3); // Length
        writer.write(header.getUnitIdentifier());
        writer.write(errorCode);
        writer.write(exceptionCode);

        return writer.toByteArray();
    }
}
