package com.gitlab.soshibby.modbus.slave.types.responses;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.utils.BufferWriter;

public class ReadDiscreteInputResponse extends ModBusMessage {

    private short byteCount;
    private boolean[] values;

    public ReadDiscreteInputResponse(ModBusHeader header) {
        super(header);
    }

    public short getByteCount() {
        return byteCount;
    }

    public void setByteCount(short byteCount) {
        this.byteCount = byteCount;
    }

    public boolean[] getValues() {
        return values;
    }

    public void setValues(boolean[] values) {
        this.values = values;
    }

    @Override
    public byte[] toByte() {
        BufferWriter writer = new BufferWriter(9 + byteCount);

        writer.write(header.getTransactionIdentifier());
        writer.write(header.getProtocolIdentifier());
        writer.write(3 + byteCount); // Length
        writer.write(header.getUnitIdentifier());
        writer.write(header.getFunctionCode());
        writer.write(byteCount);

        for (int i = 0; i < byteCount; i++) {
            byte[] byteData = new byte[2];

            for (int j = 0; j < 8; j++) {
                byte boolValue = (byte) (values[i * 8 + j] ? 1 : 0);

                byteData[1] = (byte) ((byteData[1]) | (boolValue << j));

                if ((i * 8 + j + 1) >= values.length) {
                    break;
                }
            }

            writer.write(byteData[1]);
        }

        return writer.toByteArray();
    }
}

