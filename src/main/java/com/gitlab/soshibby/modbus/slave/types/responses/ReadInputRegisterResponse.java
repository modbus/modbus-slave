package com.gitlab.soshibby.modbus.slave.types.responses;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.utils.BufferWriter;

public class ReadInputRegisterResponse extends ModBusMessage {

    private short byteCount;
    private int[] values;

    public ReadInputRegisterResponse(ModBusHeader header) {
        super(header);
    }

    public short getByteCount() {
        return byteCount;
    }

    public void setByteCount(short byteCount) {
        this.byteCount = byteCount;
    }

    public int[] getValues() {
        return values;
    }

    public void setValues(int[] values) {
        this.values = values;
    }

    @Override
    public byte[] toByte() {
        BufferWriter writer = new BufferWriter(9 + byteCount);

        writer.write(header.getTransactionIdentifier());
        writer.write(header.getProtocolIdentifier());
        writer.write(3 + byteCount); // Length
        writer.write(header.getUnitIdentifier());
        writer.write(header.getFunctionCode());
        writer.write(byteCount);
        writer.write(values);

        return writer.toByteArray();
    }

}
