package com.gitlab.soshibby.modbus.slave.types.responses;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.utils.BufferWriter;

public class WriteMultipleCoilsResponse extends ModBusMessage {

    private int startingAddress;
    private int quantity;

    public WriteMultipleCoilsResponse(ModBusHeader header) {
        super(header);
    }

    public int getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress = startingAddress;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public byte[] toByte() {
        BufferWriter writer = new BufferWriter(12);

        writer.write(header.getTransactionIdentifier());
        writer.write(header.getProtocolIdentifier());
        writer.write(6); // Length
        writer.write(header.getUnitIdentifier());
        writer.write(header.getFunctionCode());
        writer.write(startingAddress);
        writer.write(quantity);

        return writer.toByteArray();
    }
}
