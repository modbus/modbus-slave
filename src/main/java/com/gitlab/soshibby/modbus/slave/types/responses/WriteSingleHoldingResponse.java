package com.gitlab.soshibby.modbus.slave.types.responses;

import com.gitlab.soshibby.modbus.slave.types.ModBusHeader;
import com.gitlab.soshibby.modbus.slave.types.ModBusMessage;
import com.gitlab.soshibby.modbus.slave.utils.BufferWriter;

public class WriteSingleHoldingResponse extends ModBusMessage {

    private int startingAddress;
    private int value;

    public WriteSingleHoldingResponse(ModBusHeader header) {
        super(header);
    }

    public int getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress = startingAddress;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public byte[] toByte() {
        BufferWriter writer = new BufferWriter(12);

        writer.write(header.getTransactionIdentifier());
        writer.write(header.getProtocolIdentifier());
        writer.write(6); // Length
        writer.write(header.getUnitIdentifier());
        writer.write(header.getFunctionCode());
        writer.write(startingAddress);
        writer.write(value);

        return writer.toByteArray();
    }
}
