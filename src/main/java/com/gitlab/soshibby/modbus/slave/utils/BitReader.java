package com.gitlab.soshibby.modbus.slave.utils;

public class BitReader {

    private byte data;
    private byte index = 0;

    public BitReader(byte data) {
        this.data = data;
    }

    public boolean nextBoolean() {
        return (data >> index++ & 0x01) == 1;
    }

    public boolean hasNext() {
        return index < 8;
    }

}
