package com.gitlab.soshibby.modbus.slave.utils;

public class BufferReader {

    private byte[] buffer;
    private int index;

    public BufferReader(byte[] buffer) {
        this.buffer = buffer;
    }

    /**
     * Returns the next byte value (8-bit) in the buffer.
     * @return byte
     */
    public byte nextByte() {
        return buffer[index++];
    }

    /**
     * Returns the next unsigned byte value (8-bit) in the buffer.
     * @return byte (as there are no unsigned byte datatype in java)
     */
    public short nextUnsignedByte() {
        return (short) ((short) buffer[index++] & 0xFF);
    }

    public boolean hasNextByte() {
        return index < buffer.length;
    }

    /**
     * Returns the next unsigned short value (16-bit) in the buffer.
     * @return int (as there are no unsigned short datatype in java)
     */
    public int nextUnsignedShort() {
        int returnValue;
        returnValue = nextUnsignedByte() << 8;
        returnValue += nextUnsignedByte();
        return returnValue;
    }

    public boolean hasNextShort() {
        return index + 1 < buffer.length;
    }

    public boolean isEndOfBuffer() {
        return !hasNextByte();
    }

    /**
     * Resets the buffer to the starting position (0).
     */
    public void reset() {
        index = 0;
    }

    /**
     * Moves to the n-th byte in the buffer.
     * @param index
     */
    public void moveTo(int index) {
        this.index = index;
    }

    public int getPosition() {
        return index;
    }

    public void setPosition(int position) {
        this.index = position;
    }

}
