package com.gitlab.soshibby.modbus.slave.utils;

import com.gitlab.soshibby.modbus.slave.exceptions.IllegalDataValueException;

public class BufferReaderAsserter {

    public static void assertHasNextShort(BufferReader reader, String onFailMessage) {
        if (!reader.hasNextShort()) {
            throw new IllegalDataValueException("End of message reached. " + onFailMessage);
        }
    }

    public static void assertHasNextByte(BufferReader reader, String onFailMessage) {
        if (!reader.hasNextByte()) {
            throw new IllegalDataValueException("End of message reached. " + onFailMessage);
        }
    }

}
