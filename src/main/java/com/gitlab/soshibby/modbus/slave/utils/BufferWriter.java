package com.gitlab.soshibby.modbus.slave.utils;

public class BufferWriter {

    private byte[] buffer;
    private int index;

    public BufferWriter(int size) {
        buffer = new byte[size];
    }

    public void write(int value) {
        buffer[index++] = (byte) ((value & 0xff00) >> 8);
        buffer[index++] = (byte) (value & 0xff);
    }

    public void write(int[] values) {
        for (int value : values) {
            write(value);
        }
    }

    public void write(short value) {
        buffer[index++] = (byte) value;
    }

    public byte[] toByteArray() {
        return buffer;
    }

}
