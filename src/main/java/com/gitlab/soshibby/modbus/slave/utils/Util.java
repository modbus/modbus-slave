package com.gitlab.soshibby.modbus.slave.utils;

public class Util {

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];

        for (int i = 0; i < bytes.length; i++) {
            int val = bytes[i] & 0xFF;
            hexChars[i * 2] = hexArray[val >>> 4];
            hexChars[i * 2 + 1] = hexArray[val & 0x0F];
        }

        return new String(hexChars);
    }
}
